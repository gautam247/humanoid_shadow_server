const express = require("express");
const app = express();
const http = require("http");
const cors = require("cors");
const { resourceUsage } = require("process");
const server = http.createServer(app);
var io = require("socket.io")(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

app.use(cors());
app.get("/", (req, res) => {
  res.send("hello");
});

var checkboxvalues, results;
var leftShoulder = [];
var rightShoulder = [];
var leftElbow = [];
var rightElbow = [];
var leftWrist = [];
var rightWrist = [];
var leftHip = [];
var rightHip = [];
var leftKnee = [];
var rightKnee = [];
var leftAnkle = [];
var rightAnkle = [];

io.on("connection", (socket) => {
  console.log("a new user connected");
  socket.on("results", (data) => {
    results = data;
    landmarkinit();
    landmarkassign(results);

    console.log(calculate_angle(leftShoulder, leftElbow, leftWrist));
  });
  socket.on("checkbox", (data) => {
    checkboxvalues = data;
    console.log(checkboxvalues);
  });
});

//////
function calculate_angle(a, b, c) {
  var radians =
    Math.atan2(c[1] - b[1], c[0] - b[0]) - Math.atan2(a[1] - b[1], a[0] - b[0]);
  angle = Math.abs((radians * 270.0) / Math.PI);

  if (angle > 270.0) {
    angle = 360 - angle;
  }
  return angle;
}
/////////////////////

function landmarkinit() {
  leftShoulder = [];
  rightShoulder = [];
  leftElbow = [];
  rightElbow = [];
  leftWrist = [];
  rightWrist = [];
  leftHip = [];
  rightHip = [];
  leftKnee = [];
  rightKnee = [];
  leftAnkle = [];
  rightAnkle = [];
}
/////////////////

function landmarkassign(results) {
  leftShoulder.push(results[11].x);
  leftShoulder.push(results[11].y);
  rightShoulder.push(results[12].x);
  rightShoulder.push(results[12].y);
  leftElbow.push(results[13].x);
  leftElbow.push(results[13].y);
  rightElbow.push(results[14].x);
  rightElbow.push(results[14].y);
  leftWrist.push(results[15].x);
  leftWrist.push(results[15].y);
  rightWrist.push(results[16].x);
  rightWrist.push(results[16].y);
  leftHip.push(results[23].x);
  leftHip.push(results[23].y);
  rightHip.push(results[24].x);
  rightHip.push(results[24].y);
  leftKnee.push(results[25].x);
  leftKnee.push(results[25].y);
  rightKnee.push(results[26].x);
  rightKnee.push(results[26].y);
  leftAnkle.push(results[27].x);
  leftAnkle.push(results[27].y);
  rightAnkle.push(results[28].x);
  rightAnkle.push(results[28].y);
}

server.listen(9000, () => {
  console.log("listening on : 9000");
});
